package com.mockito.demo.web;

import com.mockito.demo.service.AuthenticationService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

public class LoginControllerTest {

    private LoginController controller; //SUT
    private AuthenticationService service; // Mock

    @Before
    public void setUp() throws Exception {
        this.service = mock(AuthenticationService.class);
        this.controller = new LoginController(this.service);

    }

    @Test
    public void testService_validUsernameAndPassword_logsInUser() {
        // arrange
        when(service.authenticate(anyString(), anyString())).thenReturn(true);
        // act
        String viewPath = controller.service("tom", "password123");
        // assert
        Assert.assertNotNull(viewPath);
        Assert.assertEquals("/home", viewPath);


    }
}