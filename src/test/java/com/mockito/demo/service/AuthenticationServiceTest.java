package com.mockito.demo.service;

import com.mockito.demo.data.UserRepository;
import junit.framework.TestCase;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.mockito.Mockito;

public class AuthenticationServiceTest extends TestCase {

    private AuthenticationService service;    //SUT

    private UserRepository repository;  //mock

    @Before
    public void setUp(){
        this.repository = Mockito.mock(UserRepository.class);
        this.service = new AuthenticationService(this.repository);

    }

    @Ignore // wont work!
    @Test
    public void testAuthenticate(){
        // arrange
       // Mockito.when((this.repository.findByUsername(Mockito.anyString()))).thenThrow(new IllegalArgumentException());
        // act
       // this.service.authenticate("harry", "harry1234");
        // assert

    }



}